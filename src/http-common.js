import axios from "axios";

export default axios.create({
    //production url
    //baseURL:"http://ec2-65-0-40-228.ap-south-1.compute.amazonaws.com:8090/api",
    //local pc url

    baseURL: "http://localhost:8090/api",
    headers: {
        "Content-type": "application/json",

    },

});