import Vue from "vue";
import Router from "vue-router";

//import SideBarMenu from "./components/SideBarMenu"
import Verification from "./components/Verification"
import TermsAndCondition from "./components/TermsAndCondition"
import VerificationSearchResult from "./components/VerificationSearchResult"
import VehicleLogin from "./components/VehicleLogin"
//terms and conditions pages
import TransUnionAutoInfo from "./components/TermsAndConditionsPages/TransUnionAutoInfo"



Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [

    /*{
      path: "/sidebar-menu",
      name: "SideBarMenu",
      component: SideBarMenu
 
    },*/
    //verification website pages
    {
      path: "/verification",
      name: "Verification",
      component: Verification

    },
    {
      path: "/termsandconditions",
      name: "TermsAndCondition",
      component: TermsAndCondition

    },
    {
      path: "/verification-searchresult",
      name: "VerificationSearchResult",
      component: VerificationSearchResult
    },

    {
      path: "/vehicle-login",
      name: "VehicleLogin",
      component: VehicleLogin
    },

    //terms and conditions pages 

    {
      path: "/transUnion-autoinfo",
      name: "TransUnionAutoInfo",
      component: TransUnionAutoInfo
    }


  ]
});